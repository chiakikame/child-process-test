const childProcess = require('child_process');

console.log('[manager] manager started');

const queue = [];
const workers = [];

for(let i = 0; i < 4; i++) {
  workers.push({
    proc: childProcess.fork('worker.js', {env: {WORKERID: i}}),
    running: false
  });
}

workers.forEach(function messageHandlerAttacher(worker) {
  worker.proc.on('message', function workerMessageHandler(workerMessage) {
    console.log(`[manager] changing status of worker ${workerMessage.id} to idle`);
    workers[workerMessage.id].running = false;
  });
});

process.on('message', function receivedJob(job) {
  console.log(`[manager] job ${job.id} received`);
  queue.push(job);
});

setInterval(function schedule() {
  if (queue.length === 0) {
    return;
  }
  
  const idleWorkers = workers.filter((worker) => !worker.running);
  if (idleWorkers.length !== 0) {
    const job = queue.shift();
    console.log(`[manager] sending job ${job.id} to worker`);
    idleWorkers[0].proc.send(job);
    idleWorkers[0].running = true;
  }
}, 200);
