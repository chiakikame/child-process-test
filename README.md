# `child-process-test`

An application for learning how to:

* Fork children with `child_process`
* Fork children in forked children
* Utilize message channels
* Organize a simple worker pool
