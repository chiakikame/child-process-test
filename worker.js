const workerId = parseInt(process.env.WORKERID);

console.log(`[worker ${workerId}]worker started`);

process.on('message', function(job) {
  console.log(`[worker ${workerId}] running job ${job.id} (weight ${job.content})`);
  setTimeout(function jobDoneMessage() {
    console.log(`[worker ${workerId}] running job ${job.id} done`);
    process.send({
      id: workerId
    });
  }, job.content * 500);
});
