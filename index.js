const childProcess = require('child_process');

let jobId = 0;

console.log('[master] master started');
const manager = childProcess.fork('manager.js');

setInterval(function() {
  jobId += 1;
  const jobContent = Math.floor(Math.random() * 10) + 1;
  
  console.log(`[master] sending job ${jobId}`);
  
  manager.send({
    id: jobId,
    content: jobContent
  });
}, 1000);
